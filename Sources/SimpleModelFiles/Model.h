#ifndef MODEL_H_
#define MODEL_H_

class Model {

private:


public:
	Model(const std::string& propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	~Model();

	void initSchedule(repast::ScheduleRunner& runner);
	void doSomething();
};

#endif
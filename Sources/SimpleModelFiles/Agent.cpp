#include "repast_hpc/AgentId.h"

#include "Agent.h"

Agent::Agent(repast::AgentId id): id_(id) {}

Agent::~Agent() {}


double Agent::getNumber() {}

double Agent::getWinningPercentage() {}

void Agent::pickNumber() {}

void Agent::play(repast::SharedContext<Agent>* context) {}

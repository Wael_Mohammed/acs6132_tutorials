#include <boost/mpi.hpp>
#include "repast_hpc/RepastProcess.h"

#include "Model.h"

Model::Model(const std::string& propsFile, int argc, char** argv, boost::mpi::communicator* comm) {}
Model::~Model() {}

void Model::doSomething() {
	std::cout << "Rank " << repast::RepastProcess::instance()->rank() << " is doing something." << std::endl;
}

void Model::initSchedule(repast::ScheduleRunner& runner) {
	runner.scheduleEvent(1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::doSomething)));
	runner.scheduleStop(3);
}

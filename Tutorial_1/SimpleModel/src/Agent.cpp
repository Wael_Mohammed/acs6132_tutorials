#include "repast_hpc/AgentId.h" //gives access to repast agents-related methods and classes.

#include "Agent.h" //links this file (Agent.cpp) to where the Agent interface/class is defined. Some member methods of class Agent were defined in the Agent.h file, the remaing member functions/methods will be defined here below.

Agent::Agent(repast::AgentId id): id_(id) {} //constructor, creates and initialses using Id.

Agent::~Agent() {}//deconstructor


double Agent::getNumber() { return myNumber; } //gets the value of myNumber

double Agent::getWinningPercentage() {
  if (totalPlay == 0){ // to avoid division by 0.
    return 0;
  } else {
    return (double)totalWin / (double)totalPlay; //both are int to have to cast to double before division (so that it is not integer division)
  }
}

void Agent::pickNumber() {
  myNumber = repast::Random::instance() -> nextDouble(); //pick a random number
}

void Agent::play(repast::SharedContext<Agent>* context) { //gets passed a pointer to a SharedContext object.
  std::vector<Agent*> agents; //create a vector agents within the pointer to class Agent
  context -> selectAgents(2, agents); //pick 2 random agents and save them to the vector agents.
  std::vector<Agent*>::iterator it = agents.begin(); //the iterator is pointing at the first item in the vector. The iterator is created within the scope of the Agent class.
  while (it != agents.end()) {
    totalPlay++; //increment totalPlay
    std::cout << "After playing started:" << std::endl;
    std::cout << "getId is: " << (*it)->getId() << " "; //added by WM to understand what is happening
    std::cout << "id_ is: " << id_ << " "; //added by WM to understand what is happening
    std::cout << "myNumber is: " << myNumber << " "; //added by WM to understand what is happening
    std::cout << "getNumber() is: " <<  (*it) -> getNumber() << std::endl; //added by WM to understand what is happening
    if (myNumber > (*it) -> getNumber()) { //compare current myNumber to the value returned by getNumber() number.
      totalWin++; //increment totalWin
      std::cout << "Agent " << id_ << "\t>\t" << "Agent " << (*it) -> getId() << std::endl;
    } else {
      std::cout << "Agent " << id_ << "\t<\t" << "Agent " << (*it) -> getId() << std::endl;
    }
    it++; //increment the iterator *it, to point to the next agent in vector agents.
  }
}

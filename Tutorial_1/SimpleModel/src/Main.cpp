#include <boost/mpi.hpp> //a boost mpi wrapper that allows scoping (using ::) class like environment and communicator
#include "repast_hpc/RepastProcess.h" //connect repast library/package

#include "Model.h" //connect Main to Model class. Model class is defined in Model.h. Model.h also declares Model member functions; however, Model.cpp defines those member functions.

/* #include <stdio.h> */ //no longer needed

int main(int argc, char** argv){ //argc provides a count of the number of arguments provided when running the model. Whereas, argv is a string array that stores the arguments. argv[0] is bin/main.exe.

	boost::mpi::environment env(argc, argv); //initiate an instance of class environmentwith the arguments argc (count of arrguments) and the arguments (argv)
	boost::mpi::communicator world; //creates an instance of class communicator, this allows information and communication with the processors

	if(argc < 3){ //argc is equal to the number of arguments given to the mpirun command when running the model from the terminal.
	    std::cerr << "There aren't enough arguments. Number of provided arguments: " << argc << std::endl; //cerr is called from with the std package/library using the scope operator (::). It pushes a message as an error.
			return -1;
	}

	std::string configFile = argv[1]; //argument 2 (at possition 1 in argv array) is the name of the configuration file. This file is used below to initialse the RepastProcess.
	std::string propsFile = argv[2]; //argument 3 (at possition 2 in argv array) is the name of the model properties file. This file is used below to initialse the Model. It provides the properties to the Model class, and can be used to change the settings of the simulation without having to recompile. (just update the model.props and do mpirun).

	repast::RepastProcess::init(configFile); //initialse RepastProcess using the configFile. (This is a static method of the RepastProcess class; it is not called on an instance, but on the class itself.). RepastProcess controls the model across different processses.
	Model* model = new Model(propsFile, argc, argv, &world); //create a pointer model that points to an instance of class Model. This also initialises the model using the properties file, the number of arguments supplied to the run command, the arguments supplied, and the reference of the communicator.
	repast::ScheduleRunner& runner = repast::RepastProcess::instance()-> getScheduleRunner(); //retrieves a handle to an object that manages the timing by which events in RepastHPC take place. Note that this handle is retrieved from the RepastProcess instance.
	model -> initAgents(); //this will create the agents and assign their Ids.
	model -> initSchedule(runner); //initiating the model's event schedule by passing the timing handler object to the model class using the Model class member function initSchedule.
	runner.run(); //starts the processing of events on the schedule.

	delete model; //trigers Model class deconstructor (~Model) which in turn removes the objects and frees any linked memory.

	repast::RepastProcess::instance() -> done(); //I think triggering the member function/method done() ends the current RepastProcess instance.

	return 0;

/*
	if(world.rank() == 0){ //the rank() in world.rank() is a member methods inhereted from class communicator. It quries the number_id of the processor excutting the code.
	    printf("Hello, world! I'm rank %d\n", world.rank());
	}
	else{
	    printf("Hmm... I am rank %d\n", world.rank());
	}
*/
}

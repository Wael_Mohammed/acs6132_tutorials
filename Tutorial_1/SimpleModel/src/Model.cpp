#include <boost/mpi.hpp> //a boost mpi wrapper that allows scoping (using ::) class like environment and communicator
#include "repast_hpc/RepastProcess.h" //connect repast library/package
#include "repast_hpc/initialize_random.h" //connect repast's random number generation package

#include "Model.h"//connect Main to Model class. Model class is defined in Model.h. Model.h also declares Model member functions; however, Model.cpp defines/implements the declared constructor, deconstructor, and two other functions.

Model::Model(const std::string& propsFile, int argc, char** argv, boost::mpi::communicator* comm) : context(comm) { //comm is passed to the SharedContext object context.
	props = new repast::Properties(propsFile, argc, argv, comm); //props was declared in Model.h as a pointer to class repast::Properties.
	stopAt = repast::strToInt(props -> getProperty("stop.at")); //stopAt is declared in Model.h as an intger variable. Here it's value is defined using the repast::strToInt() function which takes a value from the repast::Properties pointer (props) member method (accessed using ->) getProperty().
	countOfAgents = repast::strToInt(props -> getProperty("count.of.agents")); //countOfAgents is declared at Model.h as an integer. It will hold the number of objects of class Agent (agents) to be created in the instance of class Model using the class member function initAgents().
	initializeRandom(*props, comm); //initialse the repast's random number generation.

} //constructor, this is used to create an instance (object) of calss Model. It also initialses the class since the parenthesis are not empty ().
Model::~Model() { //deconstructor, called to remove the instance of an object of class Model
	delete props; //this will likely triger the deconstructor for class repast::Properties.

} //this will be executed once an object of class model is deleted.

void Model::initAgents(){
	int rank = repast::RepastProcess::instance()-> rank(); //gets the number_id of the processor on which the code is running.
	for (int i = 0; i < countOfAgents; i++) {
		repast::AgentId id(i, rank, 0); //create the object id of class AgentId. The value 0 is the type of the agent.
		id.currentRank(rank); //use the member function currentRank to assign the current rank. This function updates as needed.
		Agent* agent = new Agent(id); //create a pointer of class Agent with id value.
		context.addAgent(agent); //add the created pointer (agent) to the object context of class SharedContext.

	}
}

void Model::doSomething() {
	std::vector<Agent*> agents;
	context.selectAgents(countOfAgents, agents); //select all agents. The 'selectAgents' method of the context object can be used to select any subset of agents from the context, and can be used to return those agents either in an arbitrary order or in a mathematically randomized order.

	//each agent pick a random number
	std::vector<Agent*>::iterator it = agents.begin(); //create an instance it of class iterator from within the scope of the vector created inside the pointer that is pointing to class Agent.
	while (it != agents.end()) { //while the iterator is not pointing at the last item in the vector.
		(*it) -> pickNumber(); //run the member function pickNumber of the agent from class Agent pointed to by the it.
		it++; //increment the iterator to point at the next item in the vector.
	}

	//each agent plays
	it = agents.begin(); //set it to point to the item at the bigining og the vector agents
	while (it != agents.end()) {
		std::cout << "Befoe playing: getId() is:" << (*it)->getId() << " " << std::endl;
		(*it) -> play(&context); //run the play member function of class Agent
		it++;
	}

	//print winning percentage
	it = agents.begin();
	while (it != agents.end()) {
		std::cout << "Agent " << (*it) -> getId() << ": " << (*it) -> getWinningPercentage() << std::endl;
		it++;
	}

} //defining a member method that can be scheduled by the timing hundler (runner - see below). It can get access to information about which 'rank' it's being run on from within Repast, using the RepastProcess::instance()->rank() method.)

void Model::initSchedule(repast::ScheduleRunner& runner) { //the runner class will shedule events in the simulation.
	runner.scheduleEvent(1, 2, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::doSomething))); //the first argument, '1', indicates that the event will occur when the simulation reaches tick/timestep '1'.
	//The second argument '2', indicates that the event reoccurs every '2' ticks.
	//The third argument is a special class, a 'repast::Schedule::FunctorPtr' that points to the 'doSomething' method of the instance of the <Model> object. By placing this pointer (FunctorPtr) on the event schedule, RepastHPC is instructed to call that method of the model instance.
	runner.scheduleStop(stopAt); //the simulation will be stoped at tick/timestep scheduleStop(n). The argument (n) can take double/int/float values or variables (stopAt) with such values.

} //ruuner is, a time handler, an object of class repast::ScheduleRunner& that manages the timing of events, Before being passed to this function, it is obtained by a call to RepastProcess::instance()->getScheduleRunner().

/* **the method below was updated above**
void Model::doSomething() {
	std::cout << "Rank " << repast::RepastProcess::instance()->rank() << " is doing something." << std::endl;
}
*/

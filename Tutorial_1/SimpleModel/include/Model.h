#ifndef MODEL_H_
#define MODEL_H_

#include "repast_hpc/Properties.h" //this allows the Model class to use the properties file Model.props.
#include "repast_hpc/SharedContext.h" //the <model> class will be the arena in which the population of agents are stored. The way that this is achieved is by providing the model class with a Repast 'SharedContext' object.

#include "Agent.h" //this provides Model.cpp with the definitions of the Agent class

class Model {

private:
	repast::Properties* props; //a pointer to calss repast::Properties
	repast::SharedContext<Agent> context; //SharedContext objects (like context) are containers that are aware that some of the agents they contain may be agents that are actually being managed by other processess. It is a container that manages objects of class Agent (agents) in a process.
	int stopAt; //an integer value that will hold the value at which the initSchedule() method below will ask the runner (time handler) to stop the model.
	int countOfAgents; //an integer value that will hold the value that will indicate to the Model the number of objects (agents) of class Agent it will encapsulate.

public:
	Model(const std::string& propsFile, int argc, char** argv, boost::mpi::communicator* comm);  //constructor, this is used to create an instance (object) of calss Model. It also initialses the class since the parenthesis are not empty ().
	~Model(); //deconstructor, this will be executed once an object of class model is deleted.

	void initAgents(); //is used to create objects of class Agent (agents) in the Model class instance.
	void initSchedule(repast::ScheduleRunner& runner); //this method schedules events and excutes methods/functions like (doSomething) at specific ticks/points in time and ends/stops the simulation at a specific time/tick. Time is handled by runner which is a refrence of class (or maybe an object of class) repast::ScheduleRunner.
	void doSomething(); //this a function that does a specific set of command(s) when run by the initSchedule() method.
};

#endif

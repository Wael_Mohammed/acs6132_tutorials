/* Demo_03_Model.cpp */

#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/Random.h"

#include "Model.h"


SchellingModel::SchellingModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm){
	props = new repast::Properties(propsFile, argc, argv, comm);
	stopAt = repast::strToInt(props->getProperty("stop.at")); //get stop.at form model.props.
	countOfAgents = repast::strToInt(props->getProperty("count.of.agents")); //get stop.at form model.props.
	boardSize = repast::strToInt(props->getProperty("board.size")); //get stop.at form model.props.
	initializeRandom(*props, comm); //initialise repast random number generator.

	//TODO: read from model.props, init the shared space
	repast::Point<double> origin(1,1); //create origin (an instance of class Point) with values (1, 1).
	repast::Point<double> extent(boardSize+1,boardSize+1); //create extent (an instance of class Point) with values (boardSize + 1, boardSize + 1). The +1 is take account of the 1 value of the origin point.
	repast::GridDimensions gd(origin, extent); //initialise an instance of class GridDimensions using the gd() function with the points origin and extent as values. The grid dimension ​ gd​ is from (1,1) to (boardSize,boardSize). The ​ origin​ (1,1) is inclusive then the dimension extends ​ extent​ units (exclusive). For example, if you want the space to be from (-100,-100) to (100,100), the origin is set to (-100,-100) and the extent is set to (201,201).

	std::vector<int> processDims; //The ​ processDims​ variable defines how the dimension is split across multiple cores
	processDims.push_back(1); //push_back adds elements to the vector processDims.
	processDims.push_back(1); //it a 1 to 1 split across cores

	discreteSpace = new repast::SharedDiscreteSpace<Agent, repast::StrictBorders, repast::SimpleAdder<Agent> >("AgentDiscreteSpace", gd, processDims, 0, comm); //the space is initialised via discreteSpace variable. The ​ processDims​ variable defines how the dimension is split across multiple cores; we used 1 core for now so it is set to 1 by 1. The second-to-last argument, in this case 0, is an important component of the construction of the space. This is the width of the buffer zone of dimensions between different cores. This will be explained further in Tutorial 2.
	context.addProjection(discreteSpace); //In the Schelling model, we will use a discrete space with strict borders. Another note is that the SharedSpace allows multiple agents in the same location, so in our implementation, agents have to check whether a cell is empty before moving to it.

	std::cout << "RANK " << repast::RepastProcess::instance()->rank() << "BOUNDS: " << discreteSpace->bounds().origin() << " " <<	discreteSpace->bounds().extents() << std::endl;

}

SchellingModel::~SchellingModel(){
	delete props;
}

void SchellingModel::initAgents(){
	//TODO: init agents with a random location and a satisfied threshold (read from model.props).
	int rank = repast::RepastProcess::instance()->rank();
	repast::IntUniformGenerator gen = repast::Random::instance()->createUniIntGenerator(1, boardSize); //random from 1 to boardSize
	int countType0 = countOfAgents/2; //half type 0
	int countType1 = countOfAgents - countType0; // the rest type 1
	double threshold = repast::strToDouble(props->getProperty("threshold"));

	for (int i = 0; i < countOfAgents; i++){
		//random a location until an empty one is found (not the most efficient)
		int xRand, yRand;
		std::vector<Agent*> agentList; //agentList is a vector container that will contain the number of agents in the space identified by xRand and yRand as in below.
		do {
			agentList.clear(); //empties the vector agentList from any previous values.
			xRand = gen.next(); yRand = gen.next(); //get new random x and y coordinates using gen which is an object of class repast::IntUniformGenerator and extends from 1 to boardSize
			discreteSpace->getObjectsAt(repast::Point<int>(xRand, yRand), agentList); //queries loaction xRand, yRand and saves the results in agentList
		} while (agentList.size() != 0); //the loop will keep excuting until agentList = 0 which means that the coordinates

		//create agent, assign type, move the agent to the randomised location
		repast::Point<int> initialLocation(xRand, yRand); //setting the initial point to which the created agent will be moved by the moveTo memebr function of the discreteSpace object which is of class repast::SharedDiscreteSpace.
		repast::AgentId id(i, rank, 0); //assign the first three elements in the agent's id.
		id.currentRank(rank); //assign the forth element of the agent's id.
		int type; //declare the variable that will contain the agent's type. Unlike the 0 value set above, this method will allow the exposure to how other characteristics can be set.

		if (countType0 > 0) { //check if all agents from type 0 were created.
			type = 0; //set this agent's type
			countType0--; //deduct one from total agents of this type
		} else {
			type = 1;
			countType1--;
		}

		Agent* agent = new Agent(id, type, threshold); //create the agent usimg the variables that had values set above (id, type, threshold)
		context.addAgent(agent); //add the created agent to the context object (of class sharedcontext)
		discreteSpace->moveTo(id, initialLocation); //place the created agent by the moveTo memebr function of the discreteSpace object which is of class repast::SharedDiscreteSpace.
	}
}

void SchellingModel::doPerTick(){
	//TODO: print average satisfaction of all agents
	double avgSatisfied = 0; //declare the variable
	std::vector<Agent*> agents; //create a vector container, agents, that will store selected agents
	context.selectAgents(repast::SharedContext<Agent>::LOCAL, countOfAgents, agents); //select all Local agents and store them in vector agent
	std::vector<Agent*>::iterator it = agents.begin(); //create an instance of class iterator and set it to point at the first element in the agent vector.
	while(it != agents.end()){ //until the iterator (*it) points at the last element
		(*it)->updateStatus(&context, discreteSpace); //excutes the Agent class memebr function/method updateStatus for the agent in vector agents who is pointed at by the iterator (*it).
		avgSatisfied += (*it)->getSatisfiedStatus(); //getSatisfiedStatus is a memebr function of class Agent and it returns isSatisfied (a boolean).
		it++; //increment the iterator, hence it points at the next agent in the vector/container agents
	}
avgSatisfied /= countOfAgents; //devide number of satisfied by number of agents to get avgSatisfied.

	//TODO: all disatisified agents move to a random location
	double currentTick = repast::RepastProcess::instance()->getScheduleRunner().currentTick(); //returns the current simulation tick.
	if(repast::RepastProcess::instance()->rank() == 0) { //for agents running on processor 1
		printf("Tick: %.1f\tAvg satisfied: %.2f\n", currentTick, avgSatisfied);
	if (currentTick==1 || currentTick==stopAt || avgSatisfied==1) //print at the beginning and the end of the simulation
		printToScreen(); //a class Model member function to output all agents coordinates to file then use another application for post-processing and visualization.
	if (avgSatisfied==1) //if all agents were satisfied, no extra movement will occur in the model, therefore, it is better to end it immeadiately.
		repast::RepastProcess::instance()->getScheduleRunner().stop(); //stop ScheduleRunner overriding initSchedule function below.
	}

	//agents move to a random location if unsatisfied
	it = agents.begin();
	while(it != agents.end()){
		if (!(*it)->getSatisfiedStatus()) //if not satisfied (not false == not unsatisfied), then move.
			(*it)->move(discreteSpace); //excute the class memebr function/method move.
		it++;
	}
}
void SchellingModel::initSchedule(repast::ScheduleRunner& runner){
	runner.scheduleEvent(1, 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<SchellingModel> (this, &SchellingModel::doPerTick)));
	runner.scheduleStop(stopAt);
} //initialise the Schedule. The first event will happen at tick 1 and then repeat every 1 tick.

void SchellingModel::printToScreen() {
	//print board to screen
	std::vector<Agent*> agentList; //vector container
	for (int i=0; i<=boardSize+1; i++) {//i represents x coordinates in the shared space
		for (int j=0; j<=boardSize+1; j++) {//j represents y coordinates in the shared space
			if (i==0 || i==boardSize+1) //if at the last or first possible x coordinate
				std::cout << "-";
			else if (j==0 || j==boardSize+1) //if at the last or first possible y coordinate
				std::cout << "|";
			else { //for all other points in between x and y values between 1 and boardSize
				agentList.clear(); //emplty the vector
				discreteSpace->getObjectsAt(repast::Point<int>(i, j), agentList); //get the number of agents (set to be no more than 1) at point (i, j) and save it in agentList.
			if (agentList.size() > 1)
				{std::cerr << "More than 1 agent per cell" << std::endl;}
			if (agentList.size() == 0)
				std::cout << " ";
			else if ((agentList.front())->getType() == 0)
				std::cout << "X";
			else if ((agentList.front())->getType() == 1)
				std::cout << ".";
			}
		}
		std::cout << std::endl;
	}
} //The printToScreen() method loops through all cells in the 2D grid and prints out a space for empty space, cross for type 0, and dot for type 1. And to have a border for the print out, so the loop will start from 0 and end with boardSize+1, then a print horizontal character for the first and last line and a vertical character for the first and last column.

#include "Agent.h"
#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Point.h"
#include <stdio.h>

Agent::Agent(repast::AgentId id, int type, double threshold): agentId(id), agentType(type), threshold(threshold) {}

Agent::~Agent() {}


void Agent::updateStatus(repast::SharedContext<Agent>* context,
		repast::SharedDiscreteSpace<Agent, repast::StrictBorders, repast::SimpleAdder<Agent> >* space){
	//TODO: find agents in the neighbourhood, count their type, compare with threshold, update status (satisfied or not)
	std::vector<Agent*> neighbourAgents; //a vector to store the neighbours
	//get agent location from the space
	std::vector<int> agentLoc;
	space->getLocation(agentId, agentLoc); //request the loaction using getLOcation of agent with id agentId and save the value in agentLoc vector.
	//get neighbours from the space (8 Moore neighbours)
	repast::Point<int> center(agentLoc); //put the location of current agent at the centre.
	repast::Moore2DGridQuery<Agent> moore2DQuery(space); //create the moore2DQuery to query space for neighbours around the current agent.
	moore2DQuery.query(agentLoc, 1, false, neighbourAgents); // the four arguments in the query() function are: the center point (agentLoc); the range (1 cell in each direction); whether to include the center cell; and the vector into which the agents in the neighboring cells will be placed (neighbourAgents). Note that here were are using a range of '1'. We could use '2', but if we needed to use a larger query we would also have to expand the buffer zones of the space to ensure that local agents could 'see' all of the possible neighbors. We are also omitting the center cell; in theory there could not be neighboring agents in that cell, but if there could be we omit this because it allows us to ensure that the agent doing the search is not included in the results.

	//count similar agents in neighbours
	int similarCount = 0; //declare the variable that will store the number of same type
	std::vector<Agent*>::iterator tempAgent = neighbourAgents.begin(); // itearator created
	while(tempAgent != neighbourAgents.end()) { //unless it points at the last element in the vector neighbourAgents
		std::vector<int> otherLoc; //vetor to hold locations of neighbors?
		space->getLocation((*tempAgent)->getId(), otherLoc); //locations of neighbour is saved
		repast::Point<int> otherPoint(otherLoc); //create a point using neighbour's locations
		if ((*tempAgent)->getType() == agentType) { //if current agent and current neighbour are of same type.
			similarCount++; //increment number of neighbours of same type
		}
		tempAgent++; //increment iterator to point at the next agent in the list of neighbours
	}

	//divide the count by the number of neighbours (not always 8)
	isSatisfied = ((double)similarCount/neighbourAgents.size() >= threshold);
}

void Agent::move(repast::SharedDiscreteSpace<Agent, repast::StrictBorders, repast::SimpleAdder<Agent> >* space){
	//TODO: find a random empty cell and move to it
	//get agent location from the space
	std::vector<int> agentLoc; //vector container to store agent's location
	space->getLocation(agentId, agentLoc); //get the location from space
	//random a location until an empty one is found
	int xMax = (int)space->bounds().extents(0) - 1;
	repast::IntUniformGenerator gen = repast::Random::instance()->createUniIntGenerator(1, xMax); //IntUniformGenerator is a generator that produces ints in the rand (1, xMax)
	std::vector<int> agentNewLoc; //a vector container to store the agent's new location.
	do {
		agentNewLoc.clear(); //clear previous data
		int xRand, yRand; //declare random x and y
		std::vector<Agent*> agentList; //a vector container
		do {
			agentList.clear();
			xRand = gen.next(); //get the next intger using the next() member method of class IntUniformGenerator represented by the instance/object gen.
			yRand = gen.next();
			space->getObjectsAt(repast::Point<int>(xRand, yRand), agentList); //get the objects found at point x,y and store them in agentList.
		} while (agentList.size() != 0); //unless there are no agents at the randomly picked loaction, repeat the loop.
		agentNewLoc.push_back(xRand); //add the x axis value to the agent's agentNewLoc vector
		agentNewLoc.push_back(yRand); //add the y axis value to the agent's agentNewLoc vector
	} while(!space->bounds().contains(agentNewLoc)); //if the new loaction stored in agentNewLoc falls outside the sharedspace (space), then repeat the loop

	//move the agent
	space->moveTo(agentId,agentNewLoc); //the SharedDiscreteSpace (space) member function moveTo will move the agent to the new loaction. 

}
